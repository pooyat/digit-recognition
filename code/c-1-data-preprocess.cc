/* ===========================================================================
   Project 2
   Problem 5-c-FIRST PART
   CS 6830 -- MACHINE LEARNING
   
   PREPROCESS DATA

   This program splits the data into two files; one for DEVELOPMENT and the other
   for TRAINING. Then, based on the file for training, it creates 10 separate
   training files for each digit, 0 through 9.

   Written in C++

   Pooya Taherkhani
   October 2016
   =========================================================================== */


#include <iostream>
#include <fstream>

using namespace std;

void split_data(ifstream &ins, ofstream &outs_dev, ofstream &outs_tra,
                int &line_count);
/* ===========================================================================
   SPLIT DATA to development and training data files:
   -- copy the first 1000 lines into a file to be used for DEVELOPMENT.
   -- copy the rest of 2823 lines into a file for TRAINING.
   TAKE IN:
   -- the input file stream
   RETURN:
   -- one output file stream for development
   -- another output file stream for training
   -- and the total number of lines processed in the input data file
   =========================================================================== */

void create_training_file_for_digit(char digit, ifstream &ins, ofstream &outs);
/* ===========================================================================
   CREATE training FILES for a particular DIGIT:
   -- setting the class to 1 for instances of that digit, and to -1 for instance
   of other digits.
   TAKE IN:
   -- a digit from 0 to 9
   -- an input file stream to be processed for that digit
   RETURN:
   -- an output file stream ready to be used as a training data file for that digit
   =========================================================================== */


int main()
{
    ifstream ins_first;         // ORIGINAL training file to be partitioned
    ofstream outs_dev,          // file containing DEVELOPMENT partition
        outs_tra;               // file containing TRAINING partition
    int line_count;             // line count of ORIGINAL training

    /* SPLIT DATA into DEVELOPMENT and TRAINING data files =================== */
    ins_first.open("../data/optdigits.tra");
    outs_dev.open("../data/digits.dev");
    outs_tra.open("../data/digits.tra");
    split_data(ins_first, outs_dev, outs_tra, line_count);
    cout << "Total lines processed: " << line_count << endl;
    ins_first.close();
    outs_dev.close();
    
    /* CREATE TRAINING DATA FILES for EACH of the 10 digits ================== */
    ifstream ins_tra;                // file containing TRAINING partition to be
                                     // preprocessed for each digit
    ofstream outs[10];               // array of files, each preprocessed for a
                                     // specific digit
    string digit_filename,           // digit-specific training filename
        base_filename,
        extension;
    char digit;
    
    base_filename = "../data/digits-";
    extension = ".tra";

    digit = '0';
    for (int i = 0; i < 10; i++) {
        digit_filename = base_filename + digit + extension;
        ins_tra.open("../data/digits.tra");
        outs[i].open(digit_filename.c_str());
        create_training_file_for_digit(digit, ins_tra, outs[i]);
        ins_tra.close();
        outs[i].close();
        digit++;
    }


    outs_tra.close();

    return (0);
}


void split_data(ifstream &ins, ofstream &outs_dev, ofstream &outs_tra, int &line_count)
{
    string cur_line;            // current line
    int i = 1;                  // line iterator

    // read the first 1000 lines of data and write it into DEVELOPMENT data file
    getline(ins, cur_line);
    outs_dev << cur_line << endl;
    while (!ins.eof() && i < 1000) {
        i++;
        getline(ins, cur_line);
        outs_dev << cur_line << endl;
    }

    // read the rest of 2823 lines of data and write it into TRAINING data file
    while (!ins.eof()) {
        i++;
        getline(ins, cur_line);
        outs_tra << cur_line << endl;        
    }

    line_count = i;             // total number of lines processed
}


void create_training_file_for_digit(char digit, ifstream &ins, ofstream &outs)
{
    char ch, prev_ch;

    ins.get(ch);
    while (!ins.eof()) {
        prev_ch = ch;
        ins.get(ch);
        if (ch != '\n' && !ins.eof())
            outs << prev_ch;
        else if (prev_ch == digit)
            outs << "1";
        else if (prev_ch != '\n') // ignore empty lines at the end of file
            outs << "-1";
    }
 }
