# ====================================================================
# Project 2
# Problem 5-e-First Part
# CS 6830 -- Machine Learning

# PREPROCESS DATA

# Reformat the raw data file to fit the format required by LIBSVM
# package.  You may want to modify the value of FILE_EXT before
# running.

# Written in Python

# Pooya Taherkhani
# May 2018
# ====================================================================

FILE_EXT = 'tes'            # can be 'tra', 'dev', 'tes' for training,
                            # development, and test data sets
import csv
with open('../data/digits.' + FILE_EXT, newline='') as csvfile:
    with open('../data/svm-digits.' + FILE_EXT, 'w') as svmfile:
        csvreader = csv.reader( csvfile, delimiter=',')
        for row in csvreader:
            svmfile.write( row[-1])
            for index in range(len(row)-1):
                svmfile.write(' ')
                svmfile.write( str(index+1))
                svmfile.write(':')
                svmfile.write( str(row[index]))
            svmfile.write('\n')
