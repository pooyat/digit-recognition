# Handwritten Digit Recognition #

The programs in this folder are written to solve digit recognition problem
using different algorithms.

For analysis, results, and discussion, read the Digit Recognition section of
REPORT.pdf in the parent directory.

## What Are the Programs ##

There is one C++ program, a number of R scripts, and a Python script in this
folder.  Also there are two Bash scripts and another Python script in the `svm`
folder.

Each file is fully documented using block comments and inline comments.  The
PDF report also explains what purpose each script serves.

## Run Programs ##

### Preprocess Data ###

To preprocess data for perceptron, type the following at a Linux command line:

```bash
$ g++ c-1-data-preprocess.cc
$ ./a.out
```

### Train and Test Perceptron ###

To run any of the R scripts in this folder, use `Rscript` command at a linux
command line.  For example, `a-b-perceptron.R` is executed as following:

```bash
$ Rscript a-b-perceptron.R
```

Before running `d-4-tune-poly-gauss.R` or `d-5-test-poly-gauss.R`, you may want to
edit the files and set `KERNEL_TYPE` to `'poly'` or `'gauss'`, and `KERNEL_VER` to
`'ker_percep'` or `'ave_ker_percep'`.

Look at the box comment at the top of each R script to see what algorithm it
trains and tests.  You can also read the equivalent numbered section in
REPORT.pdf for analysis and results of each of the R scripts.

### Preprocess Data ###

To preprocess data for SVM, type:

```bash
$ python3 e-1-preprocess-data.py
```

Before running `e-1-preprocess-data.py`, you can edit the file and set FILE_EXT to
`'tra'`, `'dev'`, or `'tes'` which represents training, development, and test
datasets, respectively.

The PDF report explains how to train and test SVMs from a linux commnad line.
Some Python and Bash scripts for SVMs are in ``code/svm/`` directory.

### R Installation Required ###

If R is *not* already installed, install R by

```bash
$ apt install r-base
```

You may need to prefix the command above with `sudo`.

If running any R script from command line as described above throws out errors
saying that some package is not installed, you can install the package as below.

```bash
$ R
> install.packages('package-name', dependencies = TRUE)
```

or

```bash
> install.packages(c('package-1', 'package-2'), dependencies = TRUE)
```


