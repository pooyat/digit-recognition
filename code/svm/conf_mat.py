# ====================================================================
# Project 2
# Problem 5-e-nth part!
# CS 6830 -- Machine Learning

# COMPUTE CONFUSION MATRIX

# Compute confusion matrix based on the LIBSVM output file and the
# true labels from the input file.  Also determine the easiest and the
# hardest digit to classify.  You may want to modify the value of
# MODEL before running.

# Written in Python

# Pooya Taherkhani
# May 2018
# ====================================================================

MODEL = 'gauss'                # can be 'linear', 'poly', or 'gauss'

import csv
import numpy as np

# compute confusion matrix
with open('svm-digits.tes.scaled', newline='') as true_file:
    with open('output-' + MODEL) as pred_file:
        true_reader = csv.reader( true_file, delimiter=' ')
        conf_mat = np.zeros((10,10), dtype=int)
        for true, pred in zip(true_reader, pred_file):
            t = int(true[0])
            y = int(pred[0])
            conf_mat[ y, t] += 1

# determine easiest and hardest digits to classify
first_col_sum = int(sum(conf_mat[:,0]))
error_min = (first_col_sum - conf_mat[0,0]) / first_col_sum
error_max = error_min
easy_digit, hard_digit = 0, 0
for i in range(1,10):
    col_sum = int(sum(conf_mat[:,i]))
    error = (col_sum - conf_mat[i,i]) / col_sum
    if error < error_min:
        error_min = error
        easy_digit = i
    elif error > error_max:
        error_max = error
        hard_digit = i

# print output
if MODEL == 'linear':
    print('Linear SVM')
elif MODEL == 'poly':
    print('Polynomial Kernel SVM')
elif MODEL == 'gauss':
    print('Gaussian Kernel SVM')

print('confusion matrix[ predicted, true]\n')
print(conf_mat)

print('\neasiest digit to classify: ', easy_digit)
print('hardest digit to classify: ', hard_digit)
