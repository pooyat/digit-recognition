# Recognize Handwritten Digits #

This project trains perceptron, kernel perceptrons, and SVM to recognize
handwritten digits represented with numeric features in ``data`` directory.

To run the programs, read the README file inside ``code`` directory.

For analysis, results, and discussion, read the Digit Recognition section of
REPORT.pdf.